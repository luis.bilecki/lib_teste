module.exports = {
    hello: () => 'hello',
    sayHello: (name) => `Hello, ${name}!`,
    getContact: (name, phone, address) => [name, phone, address].join('|'),
}